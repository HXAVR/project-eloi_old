package au.com.eloi.Client;

import java.nio.FloatBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.LWJGLException;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.lwjgl.opengl.GL11;
import org.lwjgl.opengl.GLContext;
import org.lwjgl.opengl.PixelFormat;
import org.lwjgl.util.glu.GLU;
import org.newdawn.slick.Color;

import au.com.eloi.Src.Constructor;
import au.com.eloi.Src.FontRenderer;
import au.com.eloi.Src.ModelDebug;
import au.com.eloi.Src.RenderEngine;
import au.com.eloi.Src.Camera;
import au.com.eloi.Src.CameraController;
import au.com.eloi.Src.RendererWorld;
import au.com.eloi.Src.TextureUV;
import au.com.eloi.Src.Tile;
import au.com.eloi.Src.World;

public class Game {
	public static int screenWidth = 900;
	public static int screenHeight = 500;
	public boolean fullScreen = false;
	public final int fieldOfView = 70;
	public final float zNear = 0.02f;
	public final float zFar= 100.0f;
	public final float fogNear = 20f;
	public final float fogFar = 200f;
	private static final Color fogColor = new Color(0.7f, 0.7f, 0.8f, 1f);
	private static boolean runGameLoop = false;
	
	boolean canSystemRunGame;
	
	public RenderEngine renderEngine;
	protected Camera camera;
	private CameraController controller;
	
	private RendererWorld worldRenderer;	
	protected World theWorld;
	
	public FontRenderer fontRenderer;
	
	//Temporary timing control stuff.
	private long lastFrame;
    private int fps;
    private long lastFPS;
    public static int debugFPS;
    public static int delta;
	
	public void start(){	
		try {
			Display.setDisplayMode(new DisplayMode(screenWidth, screenHeight));
			Display.setFullscreen(fullScreen);
			PixelFormat pixelFormat = new PixelFormat();
			Display.create(pixelFormat);
			
		} catch (LWJGLException ex){
			ex.printStackTrace();
			Display.destroy();
			System.exit(1);
		}
		
		GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glLoadIdentity();
		GLU.gluPerspective(fieldOfView, (float)screenWidth / (float)screenHeight, zNear, zFar);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);
		GL11.glLoadIdentity();
		
		GL11.glEnable(GL11.GL_DEPTH_TEST);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_ALPHA_TEST);
		GL11.glBlendFunc(GL11.GL_SRC_ALPHA, GL11.GL_ONE_MINUS_SRC_ALPHA);
		GL11.glBlendFunc(GL11.GL_PERSPECTIVE_CORRECTION_HINT, GL11.GL_NICEST);
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glCullFace(GL11.GL_BACK);
		GL11.glEnable(GL11.GL_FOG);
		GL11.glClearColor(0f, 0f, 0f, 1f);
		
		FloatBuffer fogColours = BufferUtils.createFloatBuffer(4);
        fogColours.put(new float[]{fogColor.r, fogColor.g, fogColor.b, fogColor.a});
        GL11.glClearColor(fogColor.r, fogColor.g, fogColor.b, fogColor.a);
        fogColours.flip();
        GL11.glFog(GL11.GL_FOG_COLOR, fogColours);
        GL11.glFogi(GL11.GL_FOG_MODE, GL11.GL_LINEAR);
        GL11.glHint(GL11.GL_FOG_HINT, GL11.GL_NICEST);
        GL11.glFogf(GL11.GL_FOG_START, fogNear);
        GL11.glFogf(GL11.GL_FOG_END, fogFar);
        GL11.glFogf(GL11.GL_FOG_DENSITY, 0.005f);
        
        canSystemRunGame = GLContext.getCapabilities().GL_ARB_vertex_buffer_object;
				
		renderEngine = new RenderEngine();
		renderEngine.loadTextures();
		
		controller = new CameraController();
		camera = new Camera(controller, 1, 1, 0, 0f, 0f, 0f);
		
		theWorld = new World();
		worldRenderer = new RendererWorld(theWorld);
		
		fontRenderer = new FontRenderer(this);
		
		getDelta();
		lastFPS = getSystemTime();
		
		runGameLoop = true;

		if (!canSystemRunGame){
			System.out.println("This computer does not support ARB_Vertex_Buffer_Objects, thus this game can not run, Sorry!");
			System.exit(1);
		}
				
		run();
	}
	
	private void run(){
		try {
			while (runGameLoop){
				GL11.glClear(GL11.GL_COLOR_BUFFER_BIT | GL11.GL_DEPTH_BUFFER_BIT);
				Game.delta = getDelta();
				runGameLoop();				
				Display.setTitle("FPS: " + debugFPS + " Delta: " + Game.delta + " X: " + camera.getX() + " Y: " + camera.getY()  + " Z: " + camera.getZ());
				Display.update();
			}
		} catch (Exception ex){
			System.exit(1);
			Display.destroy();
		} finally {
			//Delete stuff
		}
	}	
	
	private void runGameLoop(){
		renderEngine.bindTexture("TerrainBasics");
		
		GL11.glDisable(GL11.GL_CULL_FACE);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
				
		/*
		Constructor var1 = Constructor.instance;
		TextureUV v = RenderEngine.getTextureMap(0).getUVCoordinates(16);
		var1.addVertexAndUV(-1, 0, 0, v.uMin, v.vMin);
		var1.addVertexAndUV(0, 0, 0, v.uMax, v.vMin);
		var1.addVertexAndUV(0, 0, 1, v.uMax, v.vMax);
		var1.addVertexAndUV(-1, 0, 1, v.uMin, v.vMax);
		
		v = RenderEngine.getTextureMap(0).getUVCoordinates(17);
		var1.addVertexAndUV(0, 0, 0, v.uMin, v.vMin);
		var1.addVertexAndUV(1, 0, 0, v.uMax, v.vMin);
		var1.addVertexAndUV(1, 0, 1, v.uMax, v.vMax);
		var1.addVertexAndUV(0, 0, 1, v.uMin, v.vMax);
		v = RenderEngine.getTextureMap(0).getUVCoordinates(18);
		var1.addVertexAndUV(1, 0, 0, v.uMin, v.vMin);
		var1.addVertexAndUV(2, 0, 0, v.uMax, v.vMin);
		var1.addVertexAndUV(2, 0, 1, v.uMax, v.vMax);
		var1.addVertexAndUV(1, 0, 1, v.uMin, v.vMax);
		v = RenderEngine.getTextureMap(0).getUVCoordinates(32);
		var1.addVertexAndUV(-1, 0, -1, v.uMin, v.vMin);
		var1.addVertexAndUV(0, 0, -1, v.uMax, v.vMin);
		var1.addVertexAndUV(0, 0, 0, v.uMax, v.vMax);
		var1.addVertexAndUV(-1, 0, 0, v.uMin, v.vMax);
		v = RenderEngine.getTextureMap(0).getUVCoordinates(33);
		var1.addVertexAndUV(1, 0, -1, v.uMin, v.vMin);
		var1.addVertexAndUV(0, 0, -1, v.uMax, v.vMin);
		var1.addVertexAndUV(0, 0, 0, v.uMax, v.vMax);
		var1.addVertexAndUV(1, 0, 0, v.uMin, v.vMax);
		v = RenderEngine.getTextureMap(0).getUVCoordinates(34);
		var1.addVertexAndUV(1, 0, -1, v.uMin, v.vMin);
		var1.addVertexAndUV(2, 0, -1, v.uMax, v.vMin);
		var1.addVertexAndUV(2, 0, 0, v.uMax, v.vMax);
		var1.addVertexAndUV(1, 0, 0, v.uMin, v.vMax);
		v = RenderEngine.getTextureMap(0).getUVCoordinates(48);
		var1.addVertexAndUV(-1, 0, -2, v.uMin, v.vMin);
		var1.addVertexAndUV(0, 0, -2, v.uMax, v.vMin);
		var1.addVertexAndUV(0, 0, -1, v.uMax, v.vMax);
		var1.addVertexAndUV(-1, 0, -1, v.uMin, v.vMax);
		v = RenderEngine.getTextureMap(0).getUVCoordinates(49);
		var1.addVertexAndUV(1, 0, -2, v.uMin, v.vMin);
		var1.addVertexAndUV(0, 0, -2, v.uMax, v.vMin);
		var1.addVertexAndUV(0, 0, -1, v.uMax, v.vMax);
		var1.addVertexAndUV(1, 0, -1, v.uMin, v.vMax);
		v = RenderEngine.getTextureMap(0).getUVCoordinates(50);
		var1.addVertexAndUV(1, 0, -2, v.uMin, v.vMin);
		var1.addVertexAndUV(2, 0, -2, v.uMax, v.vMin);
		var1.addVertexAndUV(2, 0, -1, v.uMax, v.vMax);
		var1.addVertexAndUV(1, 0, -1, v.uMin, v.vMax);*/
		
		worldRenderer.render();
		
		fontRenderer.drawString("STRING BITCH", 6, 6, 10);
		
		GL11.glEnable(GL11.GL_CULL_FACE);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
		
		
		
		GL11.glLoadIdentity();
		GL11.glRotatef(camera.getRollRotation(), 0, 0, 1);
		GL11.glRotatef(camera.getYawRotation(), 0, 1, 0);
		GL11.glRotatef(camera.getPitchRotation(), 1, 0, 0);
		GL11.glTranslatef(-camera.getX(), -camera.getY(), camera.getZ());
		GL11.glScalef(camera.getZoom(), camera.getZoom(), camera.getZoom());

		camera.update();		
		
		while(Keyboard.next()){
			if (Keyboard.getEventKeyState()) {
				if (Keyboard.getEventKey() == Keyboard.KEY_W) {
					controller.moveFoward = true;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_S) {
					controller.moveBackward = true;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_A) {
					controller.moveLeft = true;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_D) {
					controller.moveRight = true;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_SPACE) {
					controller.moveUp = true;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_LSHIFT) {
					controller.moveDown = true;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_EQUALS){
					controller.zoomIn = true;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_MINUS){
					controller.zoomOut = true;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_0){
					camera.setZoom(1f);
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_NUMPAD9){
					controller.rollPlus = true;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_NUMPAD7){
					controller.rollMinus = true;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_NUMPAD8){
					controller.pitchMinus = true;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_NUMPAD2){
					controller.pitchPlus = true;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_NUMPAD4){
					controller.yawMinus = true;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_NUMPAD6){
					controller.yawPlus = true;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_NUMPAD5){
					camera.setCameraRotation(0f, 0f, 0f);
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_LCONTROL){
					controller.speedMultiplier = true;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE){
					runGameLoop = false;
				}
			} else {
				if (Keyboard.getEventKey() == Keyboard.KEY_W) {
					controller.moveFoward = false;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_S) {
					controller.moveBackward = false;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_A) {
					controller.moveLeft = false;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_D) {
					controller.moveRight = false;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_SPACE) {
					controller.moveUp = false;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_LSHIFT) {
					controller.moveDown = false;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_EQUALS){
					controller.zoomIn = false;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_MINUS){
					controller.zoomOut = false;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_NUMPAD9){
					controller.rollPlus = false;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_NUMPAD7){
					controller.rollMinus = false;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_NUMPAD8){
					controller.pitchMinus = false;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_NUMPAD2){
					controller.pitchPlus = false;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_NUMPAD4){
					controller.yawMinus = false;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_NUMPAD6){
					controller.yawPlus = false;
				}
				if (Keyboard.getEventKey() == Keyboard.KEY_LCONTROL){
					controller.speedMultiplier = false;
				}
			}
		}
		if (Display.isCloseRequested()){
			runGameLoop = false;
		}

		updateFPS();
	}

	public static void main(String[] args) {
		Game game = new Game();
		if (args.length > 0){
			if (args[0] == "fullscreen"){
				game.fullScreen = true;
			}
		}
		game.start();
	}
	
	public int getDelta() 
    {
        long time = getSystemTime();
	    int delta = (int) (time - lastFrame);
	    lastFrame = time;
	    Game.delta = delta;

	    return delta;
    }
    
    public static long getSystemTime()
    {
        return (Sys.getTime() * 1000) / Sys.getTimerResolution();
    }
	
    public void updateFPS() 
    {
        if (getSystemTime() - lastFPS > 1000)
        {
            debugFPS = fps;
            fps = 0;
            lastFPS += 1000;
	    }
        fps++;
    }
}