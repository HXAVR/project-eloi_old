package au.com.eloi.Src;

import org.lwjgl.opengl.GL11;
import org.lwjgl.util.glu.GLU;

import au.com.eloi.Client.Game;

public class FontRenderer {
	private TextureMap fontMap;
	private Game theGame;
	
	public FontRenderer(Game game){
		this.theGame = game;
		this.fontMap = RenderEngine.getTextureMap(1);
	}
	
	public void drawString(String string, float xPosition, float yPosition, float zPosition){
		
		/*GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glPushMatrix();
		GL11.glLoadIdentity();
		GLU.gluOrtho2D(0, Game.screenWidth, 0, Game.screenHeight);
		GL11.glScalef(1, -1, 1);
		GL11.glTranslatef(0, Game.screenHeight, 0);
		GL11.glMatrixMode(GL11.GL_MODELVIEW);*/
		
		char[] characters = string.toCharArray();
		int mapValue;
		TextureUV uv;
		theGame.renderEngine.bindTexture("Font");
		Constructor var1 = Constructor.instance;
		for (int i = 0; i < characters.length; i++){
			mapValue = (int)(characters[i]) - 65;
			if (mapValue == -33){
			} else if (mapValue > -31 && mapValue < 0){
				mapValue += 58;
			}
			uv = RenderEngine.getTextureMap(1).getUVCoordinates(mapValue);
			drawCharacter(var1, xPosition, yPosition, zPosition, uv);
			xPosition += 0.2f;
		}				
		/*GL11.glMatrixMode(GL11.GL_PROJECTION);
		GL11.glPopMatrix();
		GL11.glMatrixMode(GL11.GL_MODELVIEW);*/		
	}
	
	private void drawCharacter(Constructor constructor, float x, float y, float z, TextureUV uv){
		GL11.glEnable(GL11.GL_BLEND);
		GL11.glEnable(GL11.GL_TEXTURE_2D);
		constructor.addVertexAndUV(x, y, 0, uv.uMin, uv.vMin);
		constructor.addVertexAndUV(x + 0.2f, y, 0, uv.uMax, uv.vMin);
		constructor.addVertexAndUV(x + 0.2f, y + 0.2f, 0, uv.uMax, uv.vMax);
		constructor.addVertexAndUV(x, y + 0.2f, 0, uv.uMin, uv.vMax);
		GL11.glDisable(GL11.GL_BLEND);
		GL11.glDisable(GL11.GL_TEXTURE_2D);
	}
}
