package au.com.eloi.Src;

public class Vertex3D {
	public float x;
	public float y;
	public float z;
	
	public Vertex3D(float xValue, float yValue, float zValue){
		this.x = xValue;
		this.y = yValue;
		this.z = zValue;
	}
}
