package au.com.eloi.Src;

public class RendererTile {
	private final Constructor constructor = Constructor.instance;

	public void renderTileByType(Tile tile){
		switch(tile.getRenderType()){
		case 0:
			//renderNormalTile(tile.getX(), tile.getY(), tile.getZ(), RenderEngine.getTextureMap(0).getUVCoordinates(tile.getId()));
			renderTile(tile);
			break;
		}
	}
	
	private void renderNormalTile(float x, float y, float z, TextureUV uv){
		float size = 0.25f;
		
		
		constructor.addVertexAndUV(x * size, y * size, z * size, uv.uMin, uv.uMax);
		
		constructor.addVertexAndUV((x * size) + size, y * size, z * size, uv.uMax, uv.vMin);
		
		constructor.addVertexAndUV((x * size) + size, y * size, (z * size) + size, uv.uMax, uv.vMax);
		
		constructor.addVertexAndUV(x * size, y * size, (z * size) + size, uv.uMin, uv.vMax);
		
		/*
		constructor.addVertexAndUV(x * size, y * size, z * size, uv.uMin, uv.vMin);
		constructor.addVertexAndUV((x * size) + size, y * size, z * size, uv.uMax, uv.vMin);
		constructor.addVertexAndUV((x * size) + size, y * size, (z * size) + size, uv.uMax, uv.vMax);
		constructor.addVertexAndUV(x * size, y * size, (z * size) + size, uv.uMin, uv.vMax);*/
	}
	
	private void renderTile(Tile tile){
		TextureUV uv = RenderEngine.getTextureMap(0).getUVCoordinates(tile.getId());
		float tileX = tile.getX();
		float tileY = tile.getY();
		float tileZ = tile.getZ();
		float size = 0.25f;
		
		constructor.addVertexAndUV(tileX * size, tileY * size, tileZ * size, uv.uMin, uv.vMin);
		constructor.addVertexAndUV((tileX * size) + size, tileY * size, tileZ * size, uv.uMax, uv.vMin);
		constructor.addVertexAndUV((tileX * size) + size, tileY * size, (tileZ * size) + size, uv.uMax, uv.vMax);
		constructor.addVertexAndUV(tileX * size, tileY * size, (tileZ * size) + size, uv.uMin, uv.vMax);
		
		/*
		constructor.addVertexAndUV(tileX * size, tileY * size, tileZ * size, uv.uMin, uv.uMax);
		
		constructor.addVertexAndUV((tileX * size) + size, tileY * size, tileZ * size, uv.uMax, uv.vMin);
		
		constructor.addVertexAndUV((tileX * size) + size, tileY * size, (tileZ * size) + size, uv.uMax, uv.vMax);
		
		constructor.addVertexAndUV(tileX * size, tileY * size, (tileZ * size) + size, uv.uMin, uv.vMax);*/
	}

}
