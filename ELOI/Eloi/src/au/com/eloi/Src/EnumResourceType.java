package au.com.eloi.Src;

public enum EnumResourceType {
	Terrain("terrain/"), Units("units/"), Interface("interface/"), Font("fonts/"); 
	
	public final String resourceDirectory;
	
	EnumResourceType(String resourceDir){
		this.resourceDirectory = resourceDir;
	}
	
}
