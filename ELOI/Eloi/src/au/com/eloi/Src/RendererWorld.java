package au.com.eloi.Src;

import org.lwjgl.opengl.GL11;

public class RendererWorld {
	private World theWorld;
	private int oglListId;
	private RendererTile tileRenderer;
	
	public RendererWorld(World world){
		theWorld = world;
		tileRenderer = new RendererTile();
		oglListId = OpenGLHelper.generateDisplayLists(1);
	}
	
	public void render(){
		GL11.glNewList(oglListId, GL11.GL_COMPILE);
		for (int x = 0; x < 15; x++){
			for (int z = 0; z < 15; z++){
				for (int y = 0; y < 15; y++){
					Tile focusedTile = theWorld.getTileAtPosition(x, y, z);
					if (focusedTile.getId() >= 0){
						tileRenderer.renderTileByType(focusedTile);
					}
				}
			}
		}
		GL11.glEndList();
		GL11.glCallList(oglListId);
	}
	
	public void renderTile(Tile tile){
		Constructor var1 = Constructor.instance;
		TextureUV uv = RenderEngine.getTextureMap(0).getUVCoordinates(16);
		float tileX = tile.getX();
		float tileY = tile.getY();
		float tileZ = tile.getZ();
		float size = 0.25f;
		
		
		var1.addVertexAndUV(tileX * size, tileY * size, tileZ * size, uv.uMin, uv.uMax);
		
		var1.addVertexAndUV((tileX * size) + size, tileY * size, tileZ * size, uv.uMax, uv.vMin);
		
		var1.addVertexAndUV((tileX * size) + size, tileY * size, (tileZ * size) + size, uv.uMax, uv.vMax);
		
		var1.addVertexAndUV(tileX * size, tileY * size, (tileZ * size) + size, uv.uMin, uv.vMax);
	}
}
