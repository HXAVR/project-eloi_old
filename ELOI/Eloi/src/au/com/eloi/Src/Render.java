package au.com.eloi.Src;

public abstract class Render {
	private RenderManager manager;
	
	public abstract void render();
	
	public void setManager(RenderManager renderManager){
		manager = renderManager;
	}
}
