package au.com.eloi.Src;

public class ByteStorage {
	private int size;
	private byte[] dataArray;
	
	public ByteStorage(int arraySize){
		dataArray = new byte[arraySize * arraySize * arraySize];
		size = arraySize;
	}
	
	public boolean set(int data, int x, int y, int z){
		boolean dataSet = false;
		int index = x + size * (y + size * z);
		if (index >= 0 && index <= size - 1){
			dataArray[index] = (byte)data;
			dataSet = true;
		}
		return dataSet;
	}
	
	public int get(int x, int y, int z){
		int index = x + size * (y + size * z);
		if (index >= 0 && index <= size - 1){
			return dataArray[index];
		}
		return 0x00;
	}
	
	public int getSize(){
		return size;
	}
}
