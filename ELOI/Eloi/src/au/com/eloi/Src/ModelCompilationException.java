package au.com.eloi.Src;

public class ModelCompilationException extends Exception {
	private static final long serialVersionUID = -1027974108377696347L;

	public ModelCompilationException(String message){
		super(message);
	}
}
