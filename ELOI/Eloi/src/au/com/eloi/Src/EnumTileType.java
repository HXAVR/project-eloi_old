package au.com.eloi.Src;

public enum EnumTileType {
	
	BasicTerrain("Basic Terrain");
		
	public final String type;
	
	EnumTileType(String theType){
		this.type = theType;
	}
}
