package au.com.eloi.Src;

public class World {
	ByteStorage primaryTileStorage;
	ByteStorage secondaryTileStorage;
	ByteStorage tileMetadataStorage;
	
	public World(){
		primaryTileStorage = new ByteStorage(16);
		secondaryTileStorage = new ByteStorage(16);
		tileMetadataStorage = new ByteStorage(16);
		
		for (int x = 0; x < 15; x++){
			for (int z = 0; z < 15; z++){
				for (int y = 0; y < 15; y++){
					this.setTileAtPosition(Tile.stone, x, y, z);
				}
			}
		}
	}
	
	public Tile getTileAtPosition(int x, int y, int z){
		Tile returnable;
		int id = primaryTileStorage.get(x, y, z);
		returnable = new Tile(((int)id));
		returnable.setCoordinates(x, y, z);
		returnable.renderType = secondaryTileStorage.get(x, y, z);		
		return returnable;
	}
	
	public boolean setTileAtPosition(Tile tile, int x, int y, int z){
		boolean dataSet = false;
		tile.setCoordinates(x, y, z);
		int id = tile.getId();
		dataSet = primaryTileStorage.set(id, x, y, z);
		int renderType = tile.getRenderType();
		dataSet = secondaryTileStorage.set(renderType, x, y, z) ? true : false;	
		return dataSet;
	}
	
	public void update(){
		
	}
}
