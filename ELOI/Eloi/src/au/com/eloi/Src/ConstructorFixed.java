package au.com.eloi.Src;

import java.nio.IntBuffer;

import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL11;

public class ConstructorFixed {
	private boolean isConstructing = false;
	private int[] oglCallIds;
	private int oglGenListCount;
	private int offset = 0;
	private boolean isColored = false;
	private boolean isTextured = false;
	private boolean canTranslate = true;
	private boolean canScale = true;
	private final float scale = 1;
	private float scaleModifier = 0;
	private float xTranslationModifier = 0;
	private float yTranslationModifier = 0;
	private float zTranslationModifier = 0;
	
	public ConstructorFixed(int oglListCount){
		oglGenListCount = oglListCount;
		oglCallIds = OpenGLHelper.generateDisplayListsAndFetchIDArray(oglListCount);
	}
	
	public void startConstruction(int drawMode){
		if (isConstructing){
			System.out.println("Already constructing!");
		} else {
			isConstructing = true;
			isColored = false;
			isTextured = false;
			GL11.glNewList(getNextList(), GL11.GL_COMPILE);
			GL11.glBegin(drawMode);
		}
	}
	
	public void endConstruction(){
		if (!isConstructing){
			System.out.println("No construction started!");
		} else {
			GL11.glEnd();
			GL11.glEndList();
			isConstructing = false;
		}
	}
	
	protected void addVertex(float x, float y, float z){
		canTranslate = false;
		canScale = false;
		GL11.glVertex3f((x + xTranslationModifier) * getFinalScale(), (y + yTranslationModifier) * getFinalScale(), (z + zTranslationModifier) * getFinalScale());
	}
	
	protected void setVertexColor(float red, float green, float blue, float alpha){
		canTranslate = false;
		canScale = false;
		GL11.glColor4f(red, green, blue, alpha);
		isColored = true;
	}
	
	protected void setVertexUVCoordinates(float u, float v){
		canTranslate = false;
		canScale = false;
		GL11.glTexCoord2f(u, v);
		isTextured = true;
	}
	
	protected void addVertexWithUV(float x, float y, float z, float u, float v){
		canTranslate = false;
		canScale = false;
		GL11.glTexCoord2f(u, v);
		GL11.glVertex3f((x + xTranslationModifier) * getFinalScale(), (y + yTranslationModifier) * getFinalScale(), (z + zTranslationModifier) * getFinalScale());
		isTextured = true;
	}
	
	protected void translate(float x, float y, float z){
		if (canTranslate){
			xTranslationModifier = x;
			yTranslationModifier = y;
			zTranslationModifier = z;
		}
	}
	
	protected void scale(float scaleValue){
		if (canScale){
			this.scaleModifier = scaleValue;
		}
	}
	
	private int getNextList(){
		int idOffset = offset;
		offset++;
		return oglCallIds[idOffset];
	}
	
	public IntBuffer getCallIntBuffer(){
		IntBuffer buffer = BufferUtils.createIntBuffer(oglGenListCount);
		buffer.put(oglCallIds);
		buffer.flip();
		return buffer;
	}
	
	private float getFinalScale(){
		return scale + scaleModifier;
	}
}
