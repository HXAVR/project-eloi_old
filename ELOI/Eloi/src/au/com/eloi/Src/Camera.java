package au.com.eloi.Src;

import au.com.eloi.Client.Game;

public class Camera {
	private CameraController controller;
	private float xPosition;
	private float yPosition;
	private float zPosition;
	private float yawRotation;
	private float pitchRotation;
	private float rollRotation;
	private float speed = 0.005f;
	private float speedMultiplier = 2f;
	private float zoom = 1f;
	private float zoomSpeed = 0.005f;
	private float rotationSpeed = 0.05f;
	private int facing = 0;
	
	public Camera(CameraController cameraController){
		controller = cameraController;
		xPosition = 0;
		yPosition = 0;
		zPosition = 0;
		yawRotation = 0;
		pitchRotation = 0;
		rollRotation = 0;
	}
	
	public Camera(CameraController cameraController, float xPos, float yPos, float zPos){
		controller = cameraController;
		this.xPosition = xPos;
		this.yPosition = yPos;
		this.zPosition = zPos;
		yawRotation = 0;
		pitchRotation = 0;
		rollRotation = 0;
	}
	
	public Camera(CameraController cameraController, float xPos, float yPos, float zPos, float yaw, float pitch, float roll){
		controller = cameraController;
		this.xPosition = xPos;
		this.yPosition = yPos;
		this.zPosition = zPos;
		yawRotation = yaw;
		pitchRotation = pitch;
		rollRotation = roll;
	}
	
	public void update(){
		float movementMultiplier = controller.speedMultiplier ? speedMultiplier : 1;
		if (controller.moveFoward){
			zPosition += (speed * Game.delta) * movementMultiplier;
		}
		if (controller.moveBackward){
			zPosition -= (speed * Game.delta) * movementMultiplier;
		}
		if (controller.moveRight){
			xPosition += (speed * Game.delta) * movementMultiplier;
		}
		if (controller.moveLeft){
			xPosition -= (speed * Game.delta) * movementMultiplier;
		}
		if (controller.moveUp){
			yPosition += (speed * Game.delta) * movementMultiplier;
		}
		if (controller.moveDown){
			yPosition -= (speed * Game.delta) * movementMultiplier;
		}
		if (controller.zoomIn){
			//zoom += zoomSpeed * Game.delta;
		}
		if (controller.zoomOut){
			//zoom -= zoomSpeed * Game.delta;
		}
		if (controller.rollPlus){
			rollRotation += rotationSpeed * Game.delta;
		}
		if (controller.rollMinus){
			rollRotation -= rotationSpeed * Game.delta;
		}
		if (controller.yawPlus){
			yawRotation += rotationSpeed * Game.delta;
			facing = (int)(360 / yawRotation);
			if (yawRotation > 360){
				yawRotation = 0;
			}
		}
		if (controller.yawMinus){
			yawRotation -= rotationSpeed * Game.delta;
			facing = (int)(360 / yawRotation);
			if (yawRotation < 0){
				yawRotation = 360;
			} 
		}
		if (controller.pitchPlus){
			pitchRotation -= rotationSpeed * Game.delta;
			if (pitchRotation < -85){
				pitchRotation = -85;
			}
		}
		if (controller.pitchMinus){
			pitchRotation += rotationSpeed * Game.delta;
			if (pitchRotation > 85){
				pitchRotation = 85;
			}
		}
	}
	
	public void updateCameraPosition(float xAddative, float yAddative, float zAddative){
		xPosition += xAddative;
		yPosition += yAddative;
		zPosition += zAddative;
	}
	
	public void updateX(float addative){
		xPosition += addative;
	}
	
	public void updateY(float addative){
		yPosition += addative;
	}
	
	public void updateZ(float addative){
		zPosition += addative;
	}
	
	public void setCameraRotation(float yaw, float pitch, float roll){
		this.yawRotation = yaw;
		this.pitchRotation = pitch;
		this.rollRotation = roll;
	}
	
	public void updateYaw(float addative){
		yawRotation += addative;
	}
	
	public void updatePitch(float addative){
		pitchRotation += addative;
	}
	
	public void updateRoll(float addative){
		rollRotation += addative;
	}
	
	public void setX(float value){
		xPosition = value;
	}
	
	public void setY(float value){
		yPosition = value;
	}
	
	public void setZ(float value){
		zPosition = value;
	}
	
	public void setZoom(float value){
		zoom = value;
	}
	
	public float getX(){
		return xPosition;
	}
	
	public float getY(){
		return yPosition;
	}
	
	public float getZ(){
		return zPosition;
	}
	
	public float getYawRotation(){
		return yawRotation;
	}
	
	public float getPitchRotation(){
		return pitchRotation;
	}
	
	public float getRollRotation(){
		return rollRotation;
	}
	
	public float getZoom(){
		return zoom;
	}
	
	public float getDistanceSquaredTo(float otherXpos, float otherYpos, float otherZpos){
		float x = xPosition - otherXpos;
		float y = yPosition - otherYpos;
		float z = zPosition - otherZpos;
		return x * x + y * y + z * z;
	}
	
	public int getFacing(){
		return facing;
	}
}
